# Alice Plex JSON Schema

[![License][license_badge]][license] [![JSON Schema][version_badge]][json]

This repository contains [JSON Schema][json] for Alice Plex.

This current version is _draft-07_.

## Schema

- [Album](https://aliceplex-schema.joshuaavalon.app/v1/album.schema.json)
- [Artist](https://aliceplex-schema.joshuaavalon.app/v1/artist.schema.json)
- [Episode](https://aliceplex-schema.joshuaavalon.app/v1/episode.schema.json)
- [Movie](https://aliceplex-schema.joshuaavalon.app/v1/movie.schema.json)
- [Show](https://aliceplex-schema.joshuaavalon.app/v1/show.schema.json)

[license]: https://gitlab.com/alice-plex/json-schema/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[version_badge]: https://img.shields.io/badge/version-draft--07-blue.svg
[json]: https://json-schema.org/
