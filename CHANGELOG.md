# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## 1.0.0

### Added

- Initial Release.
